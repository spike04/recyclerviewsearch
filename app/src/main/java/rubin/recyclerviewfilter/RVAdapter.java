package rubin.recyclerviewfilter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rgh on 11/17/2015.
 */
public class RVAdapter extends RecyclerView.Adapter<RVAdapter.MyHolder> {

    private List<CountryModel> mCountryModel;

    public RVAdapter(List<CountryModel> mCountryModel) {
        this.mCountryModel = mCountryModel;
    }

    @Override
    public void onBindViewHolder(MyHolder itemViewHolder, int i) {
        final CountryModel model = mCountryModel.get(i);
        itemViewHolder.country.setText(model.getName());
        itemViewHolder.isoCode.setText(model.getIsocode());
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_row, viewGroup, false);
        return new MyHolder(view);
    }

    @Override
    public int getItemCount() {
        return mCountryModel.size();
    }

    public void setFilter(List<CountryModel> countryModels) {
        mCountryModel = new ArrayList<>();
        mCountryModel.addAll(countryModels);
        notifyDataSetChanged();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        private TextView country;
        private TextView isoCode;

        public MyHolder(View view) {
            super(view);
            country = (TextView) view.findViewById(R.id.country_name);
            isoCode = (TextView) view.findViewById(R.id.country_iso);
        }
    }
}
